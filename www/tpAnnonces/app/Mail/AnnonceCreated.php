<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Annonce;

class AnnonceCreated extends Mailable
{
    use Queueable, SerializesModels;

    private $annonce;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Annonce $annonce)
    {
        $this->annonce= $annonce;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Annonce céée avec succès')
        ->view('emails.annoncecreated')->with('annonce', $this->annonce);
    }
}
