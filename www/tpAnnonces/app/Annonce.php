<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Annonce extends Model
{
        protected $fillable = [
            'titre',
            'description',
            'pays',
            'ville',
            'codePostal',
            'nombrePieces',
            'prix',
            'devise',
            'image',
            'contact',
            'email',
            'dateFinParution',
            'token',
            'isActif' 
        ];
}
