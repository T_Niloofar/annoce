<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DeleteExpiredAnnonces extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DeleteExpiredAnnonces:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'supprimer les annonces dont la
    date de fin de parution est inférieure à la date du jour';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Selectionner les annonces dont la date de fin de parution est inférieure à la date du jour et les suprimer.  
        DB::table('annonces')->where('dateFinParution', '<=', now())->delete();
    }
}
