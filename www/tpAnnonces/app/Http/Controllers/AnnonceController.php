<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Annonce;
use Illuminate\Support\Facades\Mail;
use App\Mail\AnnonceCreated;


class AnnonceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $annonces = Annonce::where("isActif", "like", "1") 
                    ->orderBy("created_at", "desc")->get();

        return view('annonces.index')->with('annonces', $annonces);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Générer la page create.blade.php et la retourner
        return view('annonces.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Valider les données du formulaire de création d'annonce
        $request-> validate([
            'titre'             =>'required|max:200',     
            'description'       =>'required|max:255', 
            'image'             =>'required', 
            'contact'           =>'required|max:255', 
            'email'             =>'required|max:200', 
            'pays'              =>'required', 
            'ville'             =>'required|max:200', 
            'dateFinParution'   =>'required',       
            'codePostal'        =>'required|integer',     
            'nombrePieces'      =>'required|integer',
            'prix'              =>'required|integer',   
            'devise'            =>'required',
            'validation'        => 'required'
            
        ]);

        $image = $request->file('image');
        $imagename = time() . '_' .$request->file('image')->getClientOriginalname(); 
        
        $imagePath= public_path('/images/');
        $image->move($imagePath,$imagename  );
 
        $annonce = new Annonce($request->all());
        //Pour inserer l'adresse de l'image dans la base de données
        $annonce->image = '/images/'.$imagename; 
        //Pour inserer un token alletoire dans la base de données
        $annonce->token = str_replace('/', '',  bcrypt(str_random(16)));
        
         
        /*
        $annonce->titre = $request->input("titre");
        $annonce->description = $request->input("description");
        $annonce->pays = $request->input("pays");
        $annonce->ville = $request->input("ville");
        $annonce->codePostal = $request->input("codePostal");
        $annonce->nombrePieces = $request->input("nombrePieces");
        $annonce->prix = $request->input("prix");
        $annonce->image = $request->input("image");
        $annonce->contact = $request->input("contact");
        $annonce->email = $request->input("email");
        $annonce->dateFinParution = $request->input("dateFinParution");

        */
        $annonce->save();
        Mail::to($request->email)->send(new AnnonceCreated($annonce));

        // Rediriger vers la apge d'accueil
        return redirect('/annonces');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //retrouver l'annonce en fonction de l'id passé en paramètre
        $annonce = Annonce::find($id);
        //Injecter l'annonce dans la page annonce
        //Générer la page annonce.blade.php et la retourner
        return view('annonces.annonce')->with ('annonce',$annonce);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
    * Filtre les annonces
    * 
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function filtrer(Request $request)
    {
        $pays = $request->input("pays");
        $ville = $request->input("ville");
        $nombrePieces = $request->input("nombrePieces");
        
        $queryBuilder = null;
        if($pays != "All"){
            $queryBuilder = Annonce::where("pays", "like", "%$pays%");
        }
        if($ville != "All"){
            if($queryBuilder == null){
                $queryBuilder = Annonce::where("ville", "like", "%$ville%");
            }else{
                $queryBuilder = $queryBuilder->where("ville", "like", "%$ville%");
            }
            
        }
        if($nombrePieces != -1){
            if($queryBuilder == null){
                $queryBuilder = Annonce::where("nombrePieces", "=", $nombrePieces);
            }else{
                $queryBuilder = $queryBuilder->where("nombrePieces", "=", $nombrePieces);
            }
        }        
        
        $annonces = $queryBuilder->orderBy("created_at", "desc")->get();
        

        // Envoyer les données vers la vue index.blade.php
        return view('annonces.index')->with('annonces', $annonces);
    }
    /**
    * Trie les annonces par prix
    * 
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function trier(Request $request)
    {
        $prix = $request->input("prix");

        $annonces = Annonce::orderBy("prix", "$prix")->get();
                                    
        // Envoyer les données vers la vue index.blade.php
        return view('annonces.index')->with('annonces', $annonces);
    }

    public function activation ($id, $token)
    { 
        $annonce = Annonce::where('id', $id)->where('token', $token)->first();
        $annonce->update(['isActif' => 1]);
        /*dd($id,$annonce);*/

        if ($annonce){
            $annonce->update(['isActif' => 1]);
            return redirect("/annonces/$id");

        }else {
            return redirect('/create')->with('error', "Cette annonce n'a pas été trouvée");

        }
    }
}
