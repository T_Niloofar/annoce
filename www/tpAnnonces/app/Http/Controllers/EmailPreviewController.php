<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Annonce;

class EmailPreviewController extends Controller
{
    public function annonceCreated($id){
        $annonce = Annonce::find($id);
         
        return view('emails.annoncecreated')->with('annonce', $annonce);
    }
}
