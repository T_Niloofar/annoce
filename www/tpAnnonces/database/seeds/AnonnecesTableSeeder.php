<?php

use Illuminate\Database\Seeder;

class AnonnecesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Annonce::class,5)->create();
    }
}
