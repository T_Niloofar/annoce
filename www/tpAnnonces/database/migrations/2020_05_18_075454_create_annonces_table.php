<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnoncesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annonces', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("titre",200);
            $table->string("description",255);
            $table->string("image",255);
            $table->string("contact",255);
            $table->string("email",100);
            $table->string("pays",100);
            $table->string("ville",100);
            $table->date("dateFinParution");
            $table->integer("codePostal");
            $table->integer("nombrePieces");
            $table->integer("prix");
            $table->string("devise",100);
            $table->string("token",100);
            $table->boolean("isActif")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('annonces');
    }
}
