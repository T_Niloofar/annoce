<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Annonce;
use Faker\Generator as Faker;

$factory->define(Annonce::class, function (Faker $faker) {
    return [
        "titre" => $faker->text(70),
        "description" => $faker->sentences(3,true),
        "image" => $faker->imageUrl($width=640,$height=480),
        "contact" => $faker->name,
        "email" => $faker->unique()->safeEmail,
        "pays" => $faker->randomElement($array =array("France","Suisse")),
        "ville" => $faker->randomElement($array =array("Geneve","Lausanne")),
        "dateFinParution" => $faker->date($format="Y-m-d",$max="now"),
        "codePostal" => $faker->numberBetween(1000,5000),
        "nombrePieces" => $faker->numberBetween(1,6),
        "prix" => $faker->numberBetween(1000,5000),
        "devise" => $faker->randomElement($array =array("EUR","CHF")),
        "token" => $faker->sha1,
        "isActif" => $faker->numberBetween(0, 1)
    ];
});
