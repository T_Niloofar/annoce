@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row col-md-12">
    
        <form action="{{ url('/annonces/filtrer') }}" class="form-inline " method="POST" >
        @csrf
        <label> Filtrer par :</label>
        <label for="pays"> Pays </label>
        <select class="custom-select m-4" name="pays">
            <option value="All" selected>Tous les pays</option>
            <option value="Suisse">Suisse</option>
            <option value="France">France</option>
        </select>
        <label for="ville"> Ville  </label>
        <select class="custom-select m-4" name="ville">
            <option value="All" selected>Toutes les villes</option>
            <option value="lausanne">Lausanne</option>
            <option value="geneve">Genève</option>
        </select>
        <label for="nombrePieces"> Nombre de pièces </label>
        <select class="custom-select m-2" name="nombrePieces">
            <option value="-1">Toutes les tailles</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6 et plus</option>
        </select>
        <button type="submit" class="btn btn-primary">Filtrer</button>
        </form>
    </div> 
</div>
<div class="container">
    <div class="d-flex justify-content-end">
        
        <form action="{{ url('/annonces/trier') }}" class="form-inline" method="POST">
        @csrf
        <label> Trier par :Prix</label>
        <select class="custom-select m-2" name="prix">
            <option value="asc">Croissant</option>
            <option value="desc">Décroissant</option>
        </select>
        <button type="submit" class="btn btn-primary">Trier</button>
        </form>
    </div>
</div>
<div class="container">    
    <div class="list-group">
        @foreach ($annonces as $annonce)
    <a href="{{URL::to('annonces/'.$annonce->id)}}">
        <div class="list-group-item">
            <div class="media">
                <img class="d-flex align-self-center mr-3" src="{{ asset("$annonce->image") }}"
                alt="image test" height="100">
                <div class="container">
                    <div class="row">
                        <h5>
                            <p>
                            {{$annonce->titre}}
                            <label class="d-flex justify-content-end">
                            {{$annonce->prix}}
                            {{$annonce->devise}}
                                TCC
                            </label>
                            </p>
                        </h5>
                    </div>
                    <div class="">
                        {{$annonce->codePostal}}
                        {{$annonce->ville}}
                    </div>
                    <div class="">
                    <p> {{$annonce->nombrePieces}} pièces</p>
                    </div>
                </div>
            </div> 
        </div>
    </a>    
        @endforeach
    </div>
</div>
    
@endsection