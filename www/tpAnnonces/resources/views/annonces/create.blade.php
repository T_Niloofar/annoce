@extends('layouts.app')

@section('content')
{{-- Javascript pour image preview --}}
<script type='text/javascript'>
    function preview_image(event) {
        var reader = new FileReader();
        reader.onload = function () {
            var output = document.getElementById('output_image');
            output.src = reader.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }
</script>

<div class="container">
    
        <div class="col-sm-12 d-flex justify-content-center py-3">
            <h1>Création d'une nouvelle annonce</h1>
        </div>

        <form action="/annonces" method="POST" enctype="multipart/form-data">
            <div class="row">
            <div class="col-lg-5">
                @if ($errors->any())
                <div class="alert alert-danger" role="alert">
                    Le formulaire contient des erreurs
                </div>
                @endif

                @csrf

                <div class="form-group row row">
                    <label class="col-sm-2" for="titre">Titre</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control @error('titre') is-invalid @enderror" id="titre" name="titre"
                        placeholder="Titre" value="{{ old('titre') }}">
                    </div>
                    @error('titre')
                    <span class="text-danger help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group row">
                    <label class="col-sm-2" for="description">Description</label>
                    <div class="col-sm-10">
                        <textarea class="form-control @error('description') is-invalid @enderror" id="description"
                        name="description" aria-label="Description de l'annonce">{{ old('description') }}</textarea>
                    </div>
                    @error('description')
                    <span class="text-danger help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group row">
                    <label class="col-sm-2" for="pays">Pays</label>
                    <div class="col-sm-10">
                        <select class="form-control @error('pays') is-invalid @enderror" id="pays" name="pays">
                            <option value="Suisse">Suisse</option>
                            <option value="France">France</option>
                        </select>
                    </div>
                    
                    @error('pays')
                    <span class="text-danger help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group row">
                    <label class="col-sm-2" for="ville">Ville</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control @error('ville') is-invalid @enderror" id="ville" name="ville" value="{{ old('ville') }}">
                    </div>
                    
                    @error('ville')
                    <span class="text-danger help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group row">
                    <label class="col-sm-2" for="codePostal">Code postal</label>
                    <div class="col-sm-10">
                       <input type="text" class="form-control @error('codePostal') is-invalid @enderror" id="codePostal"
                        name="codePostal" placeholder=" ex: 1217 ou 01600" value="{{ old('codePostal') }}"> 
                    </div>
                    
                    @error('codePostal')
                    <span class="text-danger help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group row">
                    <label class="col-sm-2" for="nombrePieces">Nombre de pièces</label>
                    <div class="col-sm-10">
                        <select class="form-control @error('nombrePieces') is-invalid @enderror" id="nombrePieces"
                            name="nombrePieces">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                        </select>   
                    </div>
                    
                    @error('nombrePieces')
                    <span class="text-danger help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group row">
                    <label class="col-sm-2" for="prix">Prix</label>
                    <div class="row col-sm-10">
                       <input type="text" class="form-control col-sm-7 @error('prix') is-invalid @enderror" id="prix" name="prix"
                        placeholder="ex: 1500" value="{{ old('prix') }}">
                    <select class="form-control col-sm-4 offset-1 @error('devise') is-invalid @enderror" id="devise" name="devise">
                        <option value="CHF">CHF</option>
                        <option value="EUR">EUR</option>
                    </select> 
                    </div>
                    
                    @error('prix')
                    <span class="text-danger help-block">{{ $message }}</span>
                    @enderror
                </div>

            </div>
            <div class="col-lg-7">
                <div class="row">
                    
                        <div class="form-group col-md-6">
                            <label for="image" class="col-form-label text-md-right">Image(s)</label>
                            <div class="">
                                <input type="file" name="image" onchange="preview_image(event)">
                                <img id="output_image" style="width: 250px; height: 150px;">
                            </div>
                        </div>
                    
                        <div class="form-group col-md-6">
                            <label for="contact" class="col-form-label text-md-right">Comment vous contacter</label>
                            <textarea class="form-control @error('contact') is-invalid @enderror" id="contact"
                                name="contact" aria-label="Comment vous contacter">{{ old('contact') }}</textarea>
                            @error('contact')
                            <span class="text-danger help-block">{{ $message }}</span>
                            @enderror
                        </div>
                    
                    
                </div>
                <div class="row">
                    <div class="form-group card p-3">
                        <div class="">
                            <p>Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page
                                avant impression.
                                Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page
                                avant impression.</p>
                            <input type="checkbox" id="checkbox" name="validation" value="true">
                            <label for="checkbox"> J'ai lu les conditions et je les ai comprises</label>
                            @error('validation')
                                <span class="text-danger help-block">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="email">Email</label>
                                <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" name="email"
                                    placeholder="email" value="{{ old('email') }}">
                                @error('email')
                                <span class="text-danger help-block">{{ $message }}</span>
                                @enderror
                            </div>
    
                            <div class="form-group col-md-6">
                                <label for="email">Date FIN parution</label>
                                <input type="date" class="form-control @error('dateFinParution') is-invalid @enderror"
                                    id="dateFinParution" name="dateFinParution"
                                    aria-label="dateFinParution" value="{{ old('dateFinParution') }}">
                                @error('dateFinParution')
                                <span class="text-danger help-block">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>

                </div>

                

                <button type="submit" class="col-lg-3 my-3 container d-flex justify-content-center btn btn-dark">Créer
                    annonce</button>

            </div>
        </div>    
        </form>

    
</div>

@endsection