@extends('layouts.app')

@section('content')

<div class="container mt-5  " >
    <div class="row">

<div class="card">
    <img src="{{$annonce->image}}" class="card-img-top" alt="...">
    <div class="card-body">
      <h5 class="card-title">{{$annonce->titre}}</h5>
      <p class="card-text">{{$annonce->description}}</p>
    </div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item">Pays :{{$annonce->pays}}</li>
      <li class="list-group-item">Ville :{{$annonce->ville}}</li>
      <li class="list-group-item">Code Postal :{{$annonce->codePostal}}</li>
      <li class="list-group-item">Nombre de pieces :{{$annonce->nombrePieces}}</li>
    </ul>
   <div class="card-body">
    <p>Comment contacter l'annonceur</p>
    <p><span>{{$annonce->contact}}</span></p>
   </div>
  </div>
       
    </div>
</div>




    
@endsection
    
