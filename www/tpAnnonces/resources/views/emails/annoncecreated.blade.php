<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

</head>
<body class="text-center">
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                    <h3>Chère Madame,Cher Monsieur</h3>
                    <p>Vous vonez de créer une annonce</p>
                    <p>Vouz pouvez l'activer en cliquant sur le lien ci-dessous<br></p>
                    </div>
                </div>
            </div>
            <div class="card-fotter">
                <div class="row d-flex justify-content-center">
                <a href="{{URL::to('annonces/actif/' . $annonce->id."/".$annonce->token)}}" class="btn btn-primary">
                 Activer votre annonce</a>

                </div>
            </div>
        </div>
    </div>
    
</body>
</html>